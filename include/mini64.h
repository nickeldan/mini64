#pragma once

#include <sys/types.h>

#define MINI64_VERSION "0.1.0"

enum mini64_ret_value {
    MINI64_RET_OK = 0,
    MINI64_RET_BAD_USAGE,
    MINI64_RET_INVALID_LENGTH,
    MINI64_RET_INVALID_CHARACTER,
    MINI64_RET_INVALID_PADDING,
};

size_t
base64_encoded_length(size_t len)
#ifdef __GNUC__
    __attribute__((const))
#endif
    ;

size_t
base64_max_decoded_length(size_t len)
#ifdef __GNUC__
    __attribute__((const))
#endif
    ;

int
base64_encode(const unsigned char *data, size_t len, char *dst);

int
base64_decode(const char *string, size_t len, unsigned char *data, size_t *data_len);
