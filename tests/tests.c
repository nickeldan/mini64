#include <string.h>

#include <scrutiny/scrutiny.h>

#include <mini64.h>

static const unsigned char DATA[9] = {0x31, 0x6c, 0x7f, 0x19, 0xe3, 0xf0, 0xc0, 0x87, 0x82};

static void
test_encoded_length(void)
{
    scrAssertEq(base64_encoded_length(1), 4);
    scrAssertEq(base64_encoded_length(2), 4);
    scrAssertEq(base64_encoded_length(3), 4);
    scrAssertEq(base64_encoded_length(4), 8);
    scrAssertEq(base64_encoded_length(32), 44);
}

static void
test_decoded_length(void)
{
    scrAssertEq(base64_max_decoded_length(4), 3);
    scrAssertEq(base64_max_decoded_length(44), 33);
}

static void
test_encode_bad_usage(void)
{
    char string[20];

    scrAssertEq(base64_encode(NULL, 0, string), MINI64_RET_BAD_USAGE);
    scrAssertEq(base64_encode(DATA, 0, NULL), MINI64_RET_BAD_USAGE);
}

static void
test_encode_zero_length(void)
{
    char string[20] = "a";

    scrAssertEq(base64_encode(DATA, 0, string), MINI64_RET_OK);
    scrAssertCharEq(string[0], '\0');
}

static void
test_encode_rem_0(void)
{
    char string[20];

    scrAssertEq(base64_encode(DATA, 9, string), MINI64_RET_OK);
    scrAssertLt(strnlen(string, sizeof(string)), sizeof(string));
    scrAssertStrEq(string, "MWx/GePwwIeC");
}

static void
test_encode_rem_1(void)
{
    char string[20];

    scrAssertEq(base64_encode(DATA, 7, string), MINI64_RET_OK);
    scrAssertLt(strnlen(string, sizeof(string)), sizeof(string));
    scrAssertStrEq(string, "MWx/GePwwA==");
}

static void
test_encode_rem_2(void)
{
    char string[20];

    scrAssertEq(base64_encode(DATA, 8, string), MINI64_RET_OK);
    scrAssertLt(strnlen(string, sizeof(string)), sizeof(string));
    scrAssertStrEq(string, "MWx/GePwwIc=");
}

static void
test_decode_bad_usage(void)
{
    size_t len;
    unsigned char data[20];

    scrAssertEq(base64_decode(NULL, 0, data, &len), MINI64_RET_BAD_USAGE);
    scrAssertEq(base64_decode("MWx/GePwwIeC", 0, NULL, &len), MINI64_RET_BAD_USAGE);
    scrAssertEq(base64_decode("MWx/GePwwIeC", 0, data, NULL), MINI64_RET_BAD_USAGE);
}

static void
test_decode_invalid_length(void)
{
    size_t len = 0;
    unsigned char data[20];

    scrAssertEq(base64_decode("abc", 3, data, &len), MINI64_RET_INVALID_LENGTH);
}

static void
test_decode_invalid_character(void)
{
    size_t len = 0;
    unsigned char data[20];

    scrAssertEq(base64_decode("_abc", 4, data, &len), MINI64_RET_INVALID_CHARACTER);
}

static void
test_decode_misplaced_equal_sign(void)
{
    size_t len = 0;
    unsigned char data[20];

    scrAssertEq(base64_decode("=abc", 4, data, &len), MINI64_RET_INVALID_CHARACTER);
    scrAssertEq(base64_decode("abcde=fg", 8, data, &len), MINI64_RET_INVALID_CHARACTER);
    scrAssertEq(base64_decode("abcdef=g", 8, data, &len), MINI64_RET_INVALID_CHARACTER);
}

static void
test_decode_invalid_padding(void)
{
    size_t len = 0;
    unsigned char data[20];

    scrAssertEq(base64_decode("AAF=", 4, data, &len), MINI64_RET_INVALID_PADDING);
    scrAssertEq(base64_decode("AB==", 4, data, &len), MINI64_RET_INVALID_PADDING);
}

static void
test_decode_rem_0(void)
{
    size_t len = 0;
    unsigned char data[20] = {0};

    scrAssertEq(base64_decode("MWx/GePwwIeC", strlen("MWx/GePwwIeC"), data, &len), MINI64_RET_OK);
    scrAssertEq(len, 9);
    scrAssertMemEq(data, DATA, 9);
}

static void
test_decode_rem_1(void)
{
    size_t len = 0;
    unsigned char data[20] = {0};

    scrAssertEq(base64_decode("MWx/GePwwA==", strlen("MWx/GePwwA=="), data, &len), MINI64_RET_OK);
    scrAssertEq(len, 7);
    scrAssertMemEq(data, DATA, 7);
}

static void
test_decode_rem_2(void)
{
    size_t len = 0;
    unsigned char data[20] = {0};

    scrAssertEq(base64_decode("MWx/GePwwIc=", strlen("MWx/GePwwIc="), data, &len), MINI64_RET_OK);
    scrAssertEq(len, 8);
    scrAssertMemEq(data, DATA, 8);
}

int
main()
{
    scrGroup group;

    group = scrGroupCreate(NULL);
#define ADD_TEST(name) scrGroupAddTest(group, #name, test_##name, NULL)
    ADD_TEST(encoded_length);
    ADD_TEST(decoded_length);
    ADD_TEST(encode_bad_usage);
    ADD_TEST(encode_zero_length);
    ADD_TEST(encode_rem_0);
    ADD_TEST(encode_rem_1);
    ADD_TEST(encode_rem_2);
    ADD_TEST(decode_bad_usage);
    ADD_TEST(decode_invalid_length);
    ADD_TEST(decode_invalid_character);
    ADD_TEST(decode_misplaced_equal_sign);
    ADD_TEST(decode_invalid_padding);
    ADD_TEST(decode_rem_0);
    ADD_TEST(decode_rem_1);
    ADD_TEST(decode_rem_2);

    return scrRun(NULL, NULL);
}
